const path = require('path')
const express = require('express')
const session = require('express-session')
const bodyParser = require('body-parser')
let {PythonShell} = require('python-shell')
const tempfile = require('tempfile')
const fs = require('fs')
const sleep = require('sleep')

// const bcrypt = require('bcryptjs')

const app = express()
const mainDirectory = path.join(__dirname,'../public')
app.use(express.static(path.join(__dirname, '../views')))
app.use(express.static(path.join(__dirname, '../relatorios')));
app.use(express.static(mainDirectory))
app.set('view engine','hbs')
app.use(bodyParser.urlencoded({extended:true}))
app.use(session({
    secret: 'oGatoPretoCruzouAEstrada',
    resave: true,
    saveUninitialized: false,
    cookie: { secure: false, maxAge: 900000 }
}))

// MONGO
const connectionURL = 'mongodb://127.0.0.1:27017'

const mongodb = require('mongodb')
const MongoClient = mongodb.MongoClient 
const databaseUsers = 'SCDF_users'

// APP
app.get('', (req,res) => {
    res.render('index')
})

app.post('/login_user', (req, res) => {
    var userID = req.body.user
    var password = req.body.password
    // let hash = bcrypt.hashSync(req.body.password, 10)
    MongoClient.connect(connectionURL, {useNewUrlParser:true}, (error,client) => {
        if (error){
            return console.log('Unable to connect to database')
        }
        var dbUsers = client.db(databaseUsers)
        var colUsers = dbUsers.collection('users')
        colUsers.findOne({'user':userID,'psswd':password}, (err, user) => {
            if (user){
                session.userID = userID
                res.redirect('home')    
            } else {
                res.render('index')
                return console.log('Unable to fetch user')
            }
        })
        return 
    }) 
})

// GERAR DOCUMENTO: https://www.npmjs.com/package/docx

app.post('/consulta_preco', (req,res) => {
    // if (!session.userID){
    //     res.render('index')
    // }
    var descItem = req.body.descItem
    PythonShell.run('./views/banco_precos_web.py', {args:[descItem]}, function (err, results) {
        if (err) throw err;
    })
    sleep.sleep(10)
    res.render('download_relatorio',{
        nome_rel:'Relatório ref a item: '+descItem, link_rel:'relatorios/relatorio_item_'+descItem+'.docx'
    })
})

app.post('/relatorio_empresa', (req,res) => {
    // if (!session.userID){
    //     res.render('index')
    // }
    var cnpj = req.body.cnpj.replace(/\./g,'').replace(/\//g,'').replace(/-/g,'')
    PythonShell.run('./views/relatorio_audesp_web.py', {args:['cnpj',cnpj]}, function (err, results) {
        if (err) throw err;
    })
    res.render('download_relatorio',{
        nome_rel:'Relatório ref a pessoa jurídica: '+cnpj, link_rel:'/relatorios/relatorio_pessoa_empresa_'+cnpj+'.docx'
    })
})

app.post('/relatorio_pessoa', (req,res) => {
    // if (!session.userID){
    //     res.render('index')
    // }
    var cpf = req.body.cpf.replace(/\./g,'').replace(/\//g,'').replace(/-/g,'')
    PythonShell.run('./views/relatorio_audesp_web.py', {args:['cpf',cpf]}, function (err, results) {
        if (err) throw err;
    })
    res.render('download_relatorio',{
        nome_rel:'Relatório ref a pessoa física: '+cpf,link_rel:'/relatorios/relatorio_pessoa_empresa_'+cpf+'.docx'
    })
})

app.post('/consulta_expressao_inv', (req,res) => {
    // if (!session.userID){
    //     res.render('index')
    // }
    var expressao = req.body.expressao
    var idInv = req.body.idInv
    // const spawn = require("child_process").spawn
    // const pythonProcess = spawn('python',["pesquisa_palavra_web.py", expressao,idInv])
    PythonShell.run('./views/pesquisa_palavra_web.py', {args:[expressao,idInv]}, function (err, results) {
        if (err) throw err;
    })
    res.render('download_relatorio',{
        nome_rel:'Relatório ref a palavra: '+expressao,link_rel:'/relatorios/'+expressao+'.docx'
    })
})

app.post('/download_relatorio', (req, res) => {
    var expressao = req.body.expressao
})

app.post('/pesquisar_investigacao', (req, res) => {
    // if (!session.userID){
    //     res.render('index')
    // }
    var idInv = req.body.idInv
    res.render('pesquisar_investigacao', {
        pesquisa_escolhida:idInv,
        lista_relatorios:[
            {'nome_rel':'Índice de documentos','link_rel':'/relatorios/indice_arquivos_investigacao_'+idInv+'.xlsx'},
            {'nome_rel':'Índice de emails','link_rel':'/relatorios/relatório_emails_investigacao_'+idInv+'.xlsx'},
            {'nome_rel':'Relatório de emails','link_rel':'/relatorios/relatório_geral_emails_'+idInv+'.docx'}
            // {'nome_rel':'Relatório de bilhetagem','link_rel':'/relatorios/relatório_bilhetagem_investigacao_'+idInv+'.txt'}
        ],

    })
})

app.get('/selecionar_inv', (req, res) => {
    if (!session.userID){
        res.render('index')
    }
    MongoClient.connect(connectionURL, {useNewUrlParser:true}, (error,client) => {
        if (error){
            return console.log('Unable to connect to database')
        }
        var dbUsers = client.db(databaseUsers)
        var colUsers = dbUsers.collection('users')
        console.log(session.userID)
        colUsers.findOne({'user':session.userID}, (error, user) => {
            if (user){
                res.render('selecionar_inv', 
                {
                    lista_investigacoes:user['investigations'],
                })
            } else{
                return console.log('Unable to fetch user')
            }
        }) 
    })  
})

app.get('/home', (req,res) => {
    // if (!session.userID){
    //     res.render('index')
    // }
    res.render('home')
})

app.get('/banco_precos', (req,res) => {
    // if (!session.userID){
    //     res.render('index')
    // }
    res.render('banco_precos')
})

app.get('/relatorio_compras_publicas', (req,res) => {
    // if (!session.userID){
    //     res.render('index')
    // }
    res.render('relatorio_compras_publicas')
})

app.listen(3333, () => {
    console.log('Funcionando')
})