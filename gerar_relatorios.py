from docx import Document
from mongo_url import mongo_url
import pymongo, pandas as pd, sys, os

def gerar_relatorios(id_inv, destination_path=os.getcwd()+'/js/views/relatorios'):
    myclient = pymongo.MongoClient(mongo_url)
    mydb = myclient["SCDF_"+id_inv]

    # RELATÓRIOS DE EMAILS
    mycol_emails = mydb["relatorios_emails_"+id_inv]
    rows_emails = []
    for d in mycol_emails.find({}):
        dic_aux = d.copy()
        del dic_aux['_id']
        rows_emails.append(dic_aux)
    df_emails = pd.DataFrame(rows_emails,index=[i for i in range(len(rows_emails))])
    df_emails.to_excel(destination_path+'/relatório_emails_investigacao_%s.xlsx' % (id_inv,))

    mycol_geral_emails = mydb["relatorios_geral_emails_"+id_inv]
    relatorio_geral_emails = ''
    for d in mycol_geral_emails.find({}):
        relatorio_geral_emails = d['relatorio_geral']
        break
    doc = Document()
    doc.add_paragraph(relatorio_geral_emails)
    doc.save(destination_path+'/relatório_geral_emails_%s.docx' % (id_inv,))

    # ÍNDICE DE ARQUIVOS
    mycol_indice_arquivos = mydb["relatorios_indice_arquivos_"+id_inv]
    rows_indice_arquivos = []
    for d in mycol_indice_arquivos.find({}):
        dic_aux = d.copy()
        del d['_id']
        rows_indice_arquivos.append(dic_aux)
    df_indice_arquivos = pd.DataFrame(rows_indice_arquivos,index=[i for i in range(len(rows_indice_arquivos))])
    df_indice_arquivos.to_excel(destination_path+'/indexação_arquivos_%s.xlsx' % (id_inv,))

if __name__ == "__main__":
    gerar_relatorios(str(sys.argv[1]), sys.argv[2])