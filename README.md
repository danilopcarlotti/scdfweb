# scdfweb

Módulo web da ferramenta SCDF.

Para instalar:

0) Baixe NodeJS, MongoDB e configure a rota nos arquivos do servidor
1) Baixe ou clone o repositório
2) Rode 'npm i' na pasta root
3) Faça a restauração do banco de dados (exclusivo para órgãos autorizados)
