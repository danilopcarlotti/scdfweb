from docx import Document
import pandas as pd, pymongo, re, pickle, sys, numpy as np, os
from scipy.spatial import distance
from sklearn.feature_extraction.text import TfidfVectorizer
from remove_accents import remove_accents
from mongoclient import MONGO_URL

myclient = pymongo.MongoClient(MONGO_URL)
mydb = myclient['BEC']
audespdb = myclient['Audesp']
banco_precos_col = audespdb['bancoPrecos']

def give_recommendation(desc_item, threshold=0.75):
    words = []
    dicionario_banco_precos = pickle.load(open(os.getcwd()+'/dicionario_banco_precos.pickle','rb'))
    tfv_fit = pickle.load(open(os.getcwd()+'/tfv_fit.pickle','rb'))
    vec_word = tfv_fit.transform([desc_item]).A[0]
    for dic in banco_precos_col.find({}):
        if len(words) > 9:
            break
        else:
            vec_comp = np.array(dic['vetor_tfidf'])
            if distance.cdist([vec_word],[vec_comp],'cosine') < threshold:
                words.append(dic['_id'])
    resultado = ''
    for word in words:
        valor_medio = np.mean(dicionario_banco_precos[word]['valores_unitarios'])
        desvio_padrao = np.std(dicionario_banco_precos[word]['valores_unitarios'])
        valor_min_em_tese = valor_medio - desvio_padrao
        valor_minimo = valor_min_em_tese if valor_min_em_tese >= 0 else 0.00
        resultado += '\t' + word + '\n\n'
        resultado += "Valor médio em compras da BEC R${:.2f}\n".format(valor_medio)
        resultado += "Valor máximo e mínimo esperado em reais em compras da BEC R${:.2f} e R${:.2f}\n\n\n".format(valor_medio+desvio_padrao,valor_minimo)
    return resultado

if __name__ == "__main__":
    doc = Document()
    doc.add_paragraph(give_recommendation(sys.argv[1]))
    try:
        doc.save(os.getcwd()+'/views/relatorios/relatorio_item_'+sys.argv[1]+'.docx')
    except:
        doc.save(os.getcwd()+'/relatorios/relatorio_item_'+sys.argv[1]+'.docx')
