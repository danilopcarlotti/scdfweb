from pymongo import MongoClient
from remove_accents import remove_accents
from functools import reduce
import sys, docx, os
from mongoclient import MONGO_URL

myclient = MongoClient(MONGO_URL)

def search_word(word, id_investigacao):
    mydb = myclient["SCDF_"+id_investigacao]
    mycol = mydb["indice_palavras_documentos_"+str(id_investigacao)]
    word = remove_accents(word).lower()
    documentos_resultados = []
    for w in word.split(' '):
        doc_res_aux = []
        word_db = mycol.find_one({'_id':w})
        if word_db:
            for doc in word_db['documents']:
                doc_res_aux.append(doc)
        documentos_resultados.append(doc_res_aux)
    lista_consolidada_documentos = list(reduce(set.intersection, [set(item) for item in documentos_resultados]))
    if len(lista_consolidada_documentos):
        # documento_relatorio = Document()
        texto = '\n\n\t1) Documentos em que a expressão ' + word + ' se encontra:\n'
        for l in lista_consolidada_documentos:
            texto += l+'\n'
            # documento_relatorio.add_paragraph(doc+'\n')
        return texto
    else:
        return 'Expressão não encontrada\n\n'

def search_word_vec(word, id_investigacao):
    word = remove_accents(word).lower()
    mydb = myclient["SCDF_"+id_investigacao]
    mycol = mydb["vetores_palavras_similares_"+str(id_investigacao)]
    texto = '\n\n\n\t2) Palavras que aparecem em contextos similares:\n\n'
    for w in word.split(' '):
        word_db = mycol.find_one({'_id':w})
        if word_db:
            texto += '\n\n\tPalavra: ' + w + '\n'
            for doc in word_db:
                if doc != '_id':
                    texto += '"'+doc+'" aparece com índice de similaridade '+str(word_db[doc]) + '\n'
    return texto

if __name__ == '__main__':
    texto = search_word(sys.argv[1],sys.argv[2]) + search_word_vec(sys.argv[1],sys.argv[2])
    doc = docx.Document()
    doc.add_paragraph(texto)
    doc.save(os.getcwd()+'/views/relatorios/'+sys.argv[1]+'.docx')